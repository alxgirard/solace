---
title: "0002"
date: 2020-10-31T00:03:39+01:00
anchor: "0002"
weight: 2
draft: false
---

{{< youtube nAjrhwL59w4 >}}

----

> I was briefly on the LIGO calibration team and let me tell you, it's pretty insane. At some point I know we were actually talking to NIST because one of our measurements (I think it was laser power) was being limited by the precision of their official standards. Like I think some physical property of gold had only been measured to a certain accuracy, and we were running up against that limit.

[Source](https://www.reddit.com/r/space/comments/jkuwh1/what_50_gravitationalwave_events_reveal_about_the/gamfpmf/)

----

![](/0002/2020-10-31-122047_1920x1080_scrot.png)

![](/0002/photo_2020-10-31_23-03-26.jpg)

![](/0002/photo_2020-10-31_23-03-48.jpg)
