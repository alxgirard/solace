---
title: "0004"
date: 2020-11-03T00:02:59+01:00
anchor: "0004"
weight: 4
draft: false
---

{{< youtube jZ_yxZwAoCg >}}

---

{{< figure
    src="/0004/dOhaPfu.jpg"
    title="power supply is max 300w, and the drain for the whole system while active was around 116w."
    link="https://www.reddit.com/r/Starlink/comments/jlpu1y/starlink_beta_field_report_drove_into_a_local/"
>}}

---

{{< figure
    src="/0004/Stellar_Graveyard_noerror_EM.png"
    title="Masses in the stellar graveyard" 
    link="https://www.ligo.org/detections/O3acatalog.php"
>}}

---

{{< figure
    src="/0004/El0oSE9XUAEWUow.jpeg"
    title="When I can think about myself objectively"
    link="https://twitter.com/Schnumn/status/1323259803641270272"
>}}

---


{{< figure
    src="/0004/Screenshot_2020-11-03.png"
    title="Spot tetaneutral.net"
    link="https://metrics.torproject.org/bubbles.html#as-exits-only"
    >}}

---

{{< figure
    src="/0004/El1AK0nVcAIcWjt.jpeg"
    title="Spot the satellite!"
    link="https://twitter.com/planet4589/status/1323358505923977217"
    >}}

---

{{< youtube 9WfZuNceFDM >}}
